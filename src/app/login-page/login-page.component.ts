import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService, AlertService } from '../service';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  loginform: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
      private alertService: AlertService) {}

  ngOnInit() {
      this.loginform = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required]
      });

      // reset login status
      this.authenticationService.logout();


      // get return url from route parameters or default to '/'
      // if (this.f.username.value === 'ajarn' && this.f.password.value === 'admin') {
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/app-confirm-registration';
      // }
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginform.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.loginform.invalid) {
          return;
      }

      this.loading = true;
      this.authenticationService.loginStudent(this.f.username.value, this.f.password.value)
          .pipe(first())
          .subscribe(
              data => {
                    this.router.navigate(['/student-view-activities']);
              },
              error => {
                  this.loading = false;
              });
      this.authenticationService.loginAdmin(this.f.username.value, this.f.password.value)
              .pipe(first())
              .subscribe(
                  data => {
                        this.router.navigate(['/app-confirm-registration']);
                  },
                  error => {
                      this.loading = false;
                  });
      this.authenticationService.loginTeacher(this.f.username.value, this.f.password.value)
              .pipe(first())
              .subscribe(
                  data => {
                        this.router.navigate(['/teacher/view']);
                  },
                  error => {
                      this.loading = false;
                  });
  }


}
