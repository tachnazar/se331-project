import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NavComponent} from './nav/nav.component';
import {ListEnrolledComponent} from './students/list-enrolled/list-enrolled.component';
import {RegisterComponent} from './students/register/register.component';
import {StudentViewActivitiesComponent} from './students/student-view-activities/student-view-activities.component';
import {FileNotFoundComponent} from './shared/file-not-found/file-not-found.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthGuard } from './guards';
import { ConfirmRegistrationComponent } from './adminstrator/confirm-registration/confirm-registration.component';
import { TeacherViewActivitiesComponent } from './teachers/teacher-view-activities/teacher-view-activities.component';
import { CreateActivityComponent } from './adminstrator/create-activity/create-activity.component';
import { ListPendingComponent } from './teachers/list-pending/list-pending.component';
import { ListStudentsEnrolledComponent } from './teachers/list-students-enrolled/list-students-enrolled.component';
import { ProfileComponent } from './students/profile/profile.component';
import { CreateTeacherComponent } from './adminstrator/create-teacher/create-teacher.component';
const appRoutes: Routes = [

  {
    path: '',
    redirectTo: 'login-page',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {path: 'register', component: RegisterComponent},
  {path: 'list-enrolled', component: ListEnrolledComponent, canActivate: [AuthGuard]},
  {path: 'student-view-activities', component: StudentViewActivitiesComponent, canActivate: [AuthGuard]},
  {path: 'login-page', component: LoginPageComponent},
  {path: 'nav', component: NavComponent, canActivate: [AuthGuard]},
  {path: 'app-confirm-registration', component: ConfirmRegistrationComponent, canActivate: [AuthGuard]},
  {path: 'teacher/view', component: TeacherViewActivitiesComponent, canActivate: [AuthGuard]},
  {path: 'admin/create-activity', component: CreateActivityComponent, canActivate: [AuthGuard]},
  {path: 'teacher/list-pending', component: ListPendingComponent, canActivate: [AuthGuard]},
  {path: 'teacher/list-students-enrolled', component: ListStudentsEnrolledComponent, canActivate: [AuthGuard]},
  {path: 'student-profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'create-teacher', component: CreateTeacherComponent, canActivate: [AuthGuard]},
  {path: '**', component: FileNotFoundComponent}


];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
