import {Component, OnInit} from '@angular/core';
import {ActivityService} from '../../service/activity/activity-service';
import {ActivatedRoute, Params} from '@angular/router';
import {Activity} from '../../models/activity';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  activity: Activity;

  constructor(private route: ActivatedRoute, private activityService: ActivityService) {
  }
  ngOnInit() {
    this.route.params
      .subscribe((params: Params) => {
        this.activityService.getActivity(+params['id'])
          .subscribe((inputActivity: Activity) => this.activity = inputActivity);
      });
  }
}

