import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {ListPendingComponent} from './list-pending/list-pending.component';
import {ListStudentsEnrolledComponent} from './list-students-enrolled/list-students-enrolled.component';
import {TeacherViewActivitiesComponent} from './teacher-view-activities/teacher-view-activities.component';
import {DetailComponent} from './detail/detail.component';


const TeacherRoutes: Routes = [
  {path: 'teacher/list-enrolled', component: ListStudentsEnrolledComponent},
  {path: 'teacher/list-pending', component: ListPendingComponent},
  {path: 'teacher/view', component: TeacherViewActivitiesComponent},
  { path: 'teacher/detail/:id', component: DetailComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(TeacherRoutes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule {
}
