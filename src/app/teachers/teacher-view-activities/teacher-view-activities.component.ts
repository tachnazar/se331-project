import { Component, OnInit } from '@angular/core';
import { Activity } from 'src/app/models/activity';
import { ActivityDataImplService } from 'src/app/service/activity/activity-data-impl.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-teacher-view-activities',
  templateUrl: './teacher-view-activities.component.html',
  styleUrls: ['./teacher-view-activities.component.css']
})
export class TeacherViewActivitiesComponent implements OnInit {
  activities: Activity[] = [];
  currentActivity: Activity;
  searchText: string;

  displayedColumns = ['id', 'name', 'description', 'location', 'date', 'period'];
  constructor(private activityService: ActivityDataImplService) {
    this.currentActivity = JSON.parse(localStorage.getItem('currentActivity'));
  }

  ngOnInit() {
    this.loadAllActivities();
  }

  loadAllActivities() {
    this.activityService.getAll().pipe(first()).subscribe(activities => {
        this.activities = activities;
    });
  }
  deleteActivity(id: number) {
    this.activityService.delete(id).pipe(first()).subscribe(() => {
        this.loadAllActivities();
    });
  }
  register() {}
}

