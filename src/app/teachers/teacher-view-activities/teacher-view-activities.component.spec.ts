import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherViewActivitiesComponent } from './teacher-view-activities.component';

describe('TeacherViewActivitiesComponent', () => {
  let component: TeacherViewActivitiesComponent;
  let fixture: ComponentFixture<TeacherViewActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherViewActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherViewActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
