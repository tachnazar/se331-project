import { Pipe, PipeTransform } from '@angular/core';
import {Activity} from './models/activity';
@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(activities: Activity[], searchText: string): Activity[] {
    if (!activities || !searchText) {
      return activities;
    }

    return activities.filter(activity =>
      activity.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
  }
}
