import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CreateActivityComponent } from './create-activity/create-activity.component';
import { ConfirmRegistrationComponent } from './confirm-registration/confirm-registration.component';

const AdminRoutes: Routes = [
  { path: 'admin/create-activity', component: CreateActivityComponent },
  { path: 'admin/confirm-registration', component: ConfirmRegistrationComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(AdminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
