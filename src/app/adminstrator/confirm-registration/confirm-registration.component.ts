import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { User } from 'src/app/models';
import { UserService, AuthenticationService } from 'src/app/service';
import { ActivityDataImplService } from 'src/app/service/activity/activity-data-impl.service';

import { Activity } from 'src/app/models/activity';


@Component({
  selector: 'app-confirm-registration',
  templateUrl: './confirm-registration.component.html',
  styleUrls: ['./confirm-registration.component.css']
})
export class ConfirmRegistrationComponent implements OnInit {
  currentUser: User;
  users: User[] = [];
  activities: Activity[] = [];
  currentActivity: Activity;

  displayedColumns: string[] = ['ID', 'Firstname', 'Lastname', 'Student Id', 'Date of Birth', 'Email', 'Accept'];
  displayedColumns2: string[] = ['ID', 'Name', 'Location', 'Description', 'Period', 'Date', 'Teacher', 'Accept'];

  constructor(private userService: UserService, private authService: AuthenticationService,
              private activityService: ActivityDataImplService) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
      this.loadAllUsers();
      this.loadAllActivities();
  }

  deleteUser(id: number) {
    this.userService.delete(id).pipe(first()).subscribe(() => {
        this.loadAllUsers();
    });
  }

  private loadAllUsers() {
      this.userService.getAll().pipe(first()).subscribe(users => {
          this.users = users;
      });
  }
  deleteActivity(id: number) {
    this.activityService.delete(id).pipe(first()).subscribe(() => {
        this.loadAllActivities();
    });
  }
  loadAllActivities() {
    this.activityService.getAll().pipe(first()).subscribe(activities => {
        this.activities = activities;
    });
  }
}
