import { Component, OnInit } from '@angular/core';
import { UserService, AlertService } from 'src/app/service';
import { User } from 'src/app/models';
import { first } from 'rxjs/operators';
import { Activity } from 'src/app/models/activity';
import { ActivityService } from 'src/app/service/activity/activity-service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivityDataImplService } from 'src/app/service/activity/activity-data-impl.service';
import { Teacher } from 'src/app/models/teacher';
import { TeacherService } from 'src/app/service/teacher.service';

@Component({
  selector: 'app-create-activity',
  templateUrl: './create-activity.component.html',
  styleUrls: ['./create-activity.component.css']
})
export class CreateActivityComponent implements OnInit {
     model: Activity = new Activity();
     activityForm: FormGroup;
     loading = false;
     submitted = false;
     teachers: Teacher[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private activityService: ActivityDataImplService,
    private router: Router,
    private alertService: AlertService,
    private teacherService: TeacherService) { }

  ngOnInit() {
    this.activityForm = this.formBuilder.group({
      name: ['', Validators.required],
      location: ['', Validators.required],
      description: ['', Validators.required],
      period: ['', Validators.required],
      date: ['', Validators.required],
      teacher: ['', Validators.required]
  });
  this.loadAllTeachers();
  }
  get f() { return this.activityForm.controls; }

  onSubmit() {
    this.submitted = true;

      // stop here if form is invalid
      if (this.activityForm.invalid) {
          return;
      }

      this.loading = true;
      this.activityService.register(this.activityForm.value)
      .pipe(first())
      .subscribe(
          data => {
              this.alertService.success('Registration successful', true);
              this.router.navigate(['/admin/create-activity']);
              this.loading = false;
          },
          error => {
              this.alertService.error(error);
              this.loading = false;
          });
    }
    loadAllTeachers() {
      this.teacherService.getAll().pipe(first()).subscribe(teachers => {
        this.teachers = teachers;
      });
    }

  }



