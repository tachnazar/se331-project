import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NavComponent} from './nav/nav.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import {CreateActivityComponent} from './adminstrator/create-activity/create-activity.component';
import {ConfirmRegistrationComponent} from './adminstrator/confirm-registration/confirm-registration.component';
import {ListPendingComponent} from './teachers/list-pending/list-pending.component';
import {RegisterComponent} from './students/register/register.component';
import {ListEnrolledComponent} from './students/list-enrolled/list-enrolled.component';
import {StudentViewActivitiesComponent} from './students/student-view-activities/student-view-activities.component';
import {ListStudentsEnrolledComponent} from './teachers/list-students-enrolled/list-students-enrolled.component';
import {TeacherViewActivitiesComponent} from './teachers/teacher-view-activities/teacher-view-activities.component';
import {StudentRoutingModule} from './students/student-routing.module';
import {FileNotFoundComponent} from './shared/file-not-found/file-not-found.component';
import {TeacherRoutingModule} from './teachers/teacher-routing.module';
import {AdminRoutingModule} from './adminstrator/adminstrator-routing.module';
import {LoginPageComponent} from './login-page/login-page.component';
import {StudentService} from './service/student/student-service';
import {StudentDataImplService} from './service/student/student-data-impl.service';
import {ActivityService} from './service/activity/activity-service';
import {ActivityDataImplService} from './service/activity/activity-data-impl.service';
import { AlertComponent } from './directives/alert.component';
import { AuthGuard } from './guards';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AlertService, AuthenticationService, UserService } from './service';
import { fakeBackendProvider, JwtInterceptor, ErrorInterceptor } from './helpers';
import { DetailComponent } from './teachers/detail/detail.component';
import { ProfileComponent } from './students/profile/profile.component';
import { CreateTeacherComponent } from './adminstrator/create-teacher/create-teacher.component';
import { AdministratorService } from './service/administrator.service';
import { TeacherService } from './service/teacher.service';

import {FilterPipe} from './filter.pipe';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatIconModule, MatInputModule,
  MatListModule, MatMenuModule, MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSidenavModule, MatTableModule,
  MatToolbarModule
} from '@angular/material';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ConfirmRegistrationComponent,
    CreateActivityComponent,
    ListPendingComponent,
    RegisterComponent,
    ListEnrolledComponent,
    StudentViewActivitiesComponent,
    ListStudentsEnrolledComponent,
    TeacherViewActivitiesComponent,
    FileNotFoundComponent,
    LoginPageComponent,
    AlertComponent,
    DetailComponent,
    ProfileComponent,
    CreateTeacherComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    StudentRoutingModule,
    TeacherRoutingModule,
    AdminRoutingModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    MatPaginatorModule
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    AdministratorService,
    TeacherService,
    ActivityDataImplService,
    {provide: StudentService, useClass: StudentDataImplService},
    // {provide: ActivityService, useClass: ActivityDataImplService},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},

    // provider used to create fake backend
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
