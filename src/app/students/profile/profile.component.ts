import { Component, OnInit } from '@angular/core';
import Student from '../../models/student';
import {StudentService} from '../../service/student/student-service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { UserService, AlertService } from 'src/app/service';
import { User } from 'src/app/models';
import { first } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  updateForm: FormGroup;
  student: Student;
  currentUser: User;
  users: User[] = [];
  showForm = new BehaviorSubject<boolean>(false);
  loading = false;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private alertService: AlertService,
    private userService: UserService,
    private router: Router,
    private formBuilder: FormBuilder ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }

  ngOnInit() {
      // this.route.params
      //   .subscribe((params: Params) => {
      //     this.studentService.getStudent(+params['id'])
      //       .subscribe((inputStudent: Student) => this.student = inputStudent);
      //   });
      this.updateForm = this.formBuilder.group({
        id: this.currentUser.id,
        firstName: [],
        lastName: [],
        image: [],
        studentId: [],
        dob: [],
        username: [],
        password: []
    });

  }
  get f() { return this.updateForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.updateForm.invalid) {
        return;
    }

    this.loading = true;
    this.userService.update(this.updateForm.value)
        .pipe(first())
        .subscribe(
            data => {
                this.alertService.success('Registration successful', true);
                this.router.navigate(['/login-page']);
            },
            error => {
                alert('not working');
                this.alertService.error(error);
                this.loading = false;
            });
}
  private loadAllUsers() {
    this.userService.getAll().pipe(first()).subscribe(users => {
        this.users = users;
    });
  }
  isShowForm() {
    this.showForm.next(true);
  }
  isHideForm() {
    this.showForm.next(false);
  }
}
