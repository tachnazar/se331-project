import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentViewActivitiesComponent } from './student-view-activities.component';

describe('StudentViewActivitiesComponent', () => {
  let component: StudentViewActivitiesComponent;
  let fixture: ComponentFixture<StudentViewActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentViewActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentViewActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
