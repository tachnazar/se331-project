import {Component, OnInit} from '@angular/core';
import Student from '../../models/student';
import {StudentDataImplService} from '../../service/student/student-data-impl.service';
import {StudentService} from '../../service/student/student-service';
import {Activity} from '../../models/activity';
import {ActivityService} from '../../service/activity/activity-service';
import {ActivityDataImplService} from 'src/app/service/activity/activity-data-impl.service';
import {first} from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material';
import { UserService } from 'src/app/service';
import { TeacherService } from 'src/app/service/teacher.service';
import { User } from 'src/app/models';
import { Teacher } from 'src/app/models/teacher';

@Component({
  selector: 'app-student-view-activities',
  templateUrl: './student-view-activities.component.html',
  styleUrls: ['./student-view-activities.component.css']
})

export class StudentViewActivitiesComponent implements OnInit {

  constructor(private activityService: ActivityDataImplService, private userService: UserService,
    private teacherService: TeacherService) {
    this.currentActivity = JSON.parse(localStorage.getItem('currentActivity'));
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }
  activities: Activity[] = [];
  currentActivity: Activity;
  currentUser: User;
  teachers: Teacher[] = [];
  selectedActivity = null;

  displayedColumns = ['id', 'name', 'description', 'location', 'date', 'period', 'teacher', 'join'];
  dataSource = new MatTableDataSource(this.activities);
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnInit() {
    this.loadAllActivities();
    this.loadAllTeachers();
  }

  loadAllActivities() {
    this.activityService.getAll().pipe(first()).subscribe(activities => {
      this.activities = activities;
    });
  }
  loadAllTeachers() {
    this.teacherService.getAll().pipe(first()).subscribe(teachers => {
      this.teachers = teachers;
    });
  }


  deleteActivity(id: number) {
    this.activityService.delete(id).pipe(first()).subscribe(() => {
      this.loadAllActivities();
    });
  }

  register() {
  }


}



