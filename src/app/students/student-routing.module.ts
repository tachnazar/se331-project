import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListEnrolledComponent } from './list-enrolled/list-enrolled.component';
import { RegisterComponent } from './register/register.component';
import { StudentViewActivitiesComponent } from './student-view-activities/student-view-activities.component';
import {ProfileComponent} from './profile/profile.component';


const StudentRoutes: Routes = [
  { path: 'student/list-enrolled', component: ListEnrolledComponent },
  { path: 'student/register', component: RegisterComponent },
  { path: 'student/student-view-activities', component: StudentViewActivitiesComponent},
  { path: 'student/profile/:id', component: ProfileComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(StudentRoutes)],
  exports: [RouterModule]
})
export class StudentRoutingModule {}
