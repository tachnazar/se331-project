import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  isAdminLoggedIn$: Observable<boolean>;
  isTeacherLoggedIn$: Observable<boolean>;
  isStudentLoggedIn$: Observable<boolean>;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver, private authService: AuthenticationService) {
    this.isAdminLoggedIn$ = this.authService.isAdminLoggedIn;
    this.isTeacherLoggedIn$ = this.authService.isTeacherLoggedIn;
    this.isStudentLoggedIn$ = this.authService.isStudentLoggedIn;

  }
  logout() {
    this.authService.logout();
  }
  }
