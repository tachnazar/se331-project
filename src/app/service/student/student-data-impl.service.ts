import {StudentService} from './student-service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import Student from '../../models/student';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class StudentDataImplService extends StudentService {

  constructor(private http: HttpClient) {
    super();
  }

  students: Student[];
  student: Student;

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>('http://localhost:3000/students');

  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>('http://localhost:3000/students/' + id);
  }

}
