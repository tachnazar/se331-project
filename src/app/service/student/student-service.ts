import {Observable} from 'rxjs';
import Student from '../../models/student';


export abstract class StudentService {
  abstract getStudents(): Observable<Student[]>;
  abstract getStudent(id: number): Observable<Student>;

}
