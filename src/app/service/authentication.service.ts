﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models';
import { UserService } from './user.service';

@Injectable()
export class AuthenticationService {
    private adminLoggedIn = new BehaviorSubject<boolean>(false); // {1}
    private teacherLoggedIn = new BehaviorSubject<boolean>(false);
    private studentLoggedIn = new BehaviorSubject<boolean>(false);

    users: User[] = [];

    get isAdminLoggedIn() {
    return this.adminLoggedIn.asObservable(); // {2}
    }
    get isTeacherLoggedIn() {
      return this.teacherLoggedIn.asObservable(); // {2}
    }
    get isStudentLoggedIn() {
        return this.studentLoggedIn.asObservable(); // {2}
    }

    constructor(private http: HttpClient, private userService: UserService) { }

    loginStudent(username: string, password: string) {
      return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username: username, password: password })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    this.studentLoggedIn.next(true);

                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            }));
    }
    loginAdmin(username: string, password: string) {
      return this.http.post<any>(`${environment.apiUrl}/admins/authenticate`, { username: username, password: password })
            .pipe(map(administrator => {
                // login successful if there's a jwt token in the response
                if (administrator && administrator.token) {
                    this.adminLoggedIn.next(true);

                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(administrator));
                }

                return administrator;
            }));
    }
    loginTeacher(username: string, password: string) {
      return this.http.post<any>(`${environment.apiUrl}/teachers/authenticate`, { username: username, password: password })
            .pipe(map(teacher => {
                // login successful if there's a jwt token in the response
                if (teacher && teacher.token) {
                    this.teacherLoggedIn.next(true);

                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(teacher));
                }

                return teacher;
            }));
    }

    logout() {
        this.adminLoggedIn.next(false);
        this.studentLoggedIn.next(false);
        this.teacherLoggedIn.next(false);
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
