import { TestBed } from '@angular/core/testing';

import { ActivityDataImplService } from './activity-data-impl.service';

describe('ActivityDataImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivityDataImplService = TestBed.get(ActivityDataImplService);
    expect(service).toBeTruthy();
  });
});
