import {Injectable} from '@angular/core';
import Student from '../../models/student';
import {Observable, of} from 'rxjs';
import {ActivityService} from './activity-service';
import {Activity} from '../../models/activity';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ActivityDataImplService {
  constructor(private http: HttpClient) {
  }
  getAll() {
    return this.http.get<Activity[]>(`${environment.apiUrl}/activities`);
  }

  getById(id: number) {
      return this.http.get(`${environment.apiUrl}/activities/` + id);
  }

  register(activity: Activity) {
      return this.http.post(`${environment.apiUrl}/activities/register`, activity);
  }

  update(activity: Activity) {
      return this.http.patch(`${environment.apiUrl}/activities/` + activity.id, activity);
  }

  delete(id: number) {
      return this.http.delete(`${environment.apiUrl}/activities/` + id);
  }
  }
