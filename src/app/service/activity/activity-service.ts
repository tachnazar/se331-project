import {Observable} from 'rxjs';
import {Activity} from '../../models/activity';


export abstract class ActivityService {
  abstract getActivities(): Observable<Activity[]>;
  abstract getActivity(id: number): Observable<Activity>;
  abstract createActivity(activity: Activity): Observable<Activity>;
}

