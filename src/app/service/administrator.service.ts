import { Injectable } from '@angular/core';
import { Administrator } from '../models/administrator';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdministratorService {

  constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Administrator[]>(`${environment.apiUrl}/admins`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/admins/` + id);
    }

    register(administrator: Administrator) {
        return this.http.post(`${environment.apiUrl}/admins/register`, administrator);
    }

    update(administrator: Administrator) {
        return this.http.patch(`${environment.apiUrl}/admins/` + administrator.id, administrator);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/admins/` + id);
    }
    getByUsername(username: string) {
      return this.http.get(`${environment.apiUrl}/admins/` + username);
    }
}
