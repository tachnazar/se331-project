import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Teacher } from '../models/teacher';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Teacher[]>(`${environment.apiUrl}/teachers`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/teachers/` + id);
    }

    register(teacher: Teacher) {
        return this.http.post(`${environment.apiUrl}/teachers/register`, teacher);
    }

    update(teacher: Teacher) {
        return this.http.patch(`${environment.apiUrl}/teachers/` + teacher.id, teacher);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/teachers/` + id);
    }
    getByUsername(username: string) {
      return this.http.get(`${environment.apiUrl}/teachers/` + username);
    }
}
