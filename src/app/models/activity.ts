export class Activity {
  id: number;
  name: string;
  location: string;
  description: string;
  period: Date;
  date: Date;
  teacher: object;
}
