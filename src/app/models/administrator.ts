export class Administrator {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
}
