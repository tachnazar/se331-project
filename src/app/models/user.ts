﻿export class User {
    id: number;
    studentId: string;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    image: string;
    dob: Date;
}
