export default class Student {
  id: number;
  studentId: string;
  name: string;
  surname: string;
  image: string;
  email: string;
  password: string;
  dob: Date;
}
