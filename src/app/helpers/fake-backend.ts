﻿import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // array in local storage for registered users
        let users: any[] = JSON.parse(localStorage.getItem('users')) || [];
        let activities: any[] = JSON.parse(localStorage.getItem('activities')) || [];
        let admins: any[] = JSON.parse(localStorage.getItem('admins')) || [];
        let teachers: any[] = JSON.parse(localStorage.getItem('teachers')) || [];


        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {

            // authenticate
            if (request.url.endsWith('/users/authenticate') && request.method === 'POST') {
                // find if any user matches login credentials
                let filteredUsers = users.filter(user => {
                    return user.username === request.body.username && user.password === request.body.password;
                });

                if (filteredUsers.length) {
                    // if login details are valid return 200 OK with user details and fake jwt token
                    let user = filteredUsers[0];
                    let body = {
                        id: user.id,
                        studentId: user.studentId,
                        username: user.username,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        dob: user.dob,
                        image: user.image,
                        token: 'fake-jwt-token'
                    };

                    return of(new HttpResponse({ status: 200, body: body }));
                } else {
                    // else return 400 bad request
                    return throwError({ error: { message: 'Username or password is incorrect' } });
                }
            }
            // authentication for admin
            if (request.url.endsWith('/admins/authenticate') && request.method === 'POST') {
              // find if any user matches login credentials
              let filteredAdmins = admins.filter(administrator => {
                  return administrator.username === request.body.username && administrator.password === request.body.password;
              });

              if (filteredAdmins.length) {
                  // if login details are valid return 200 OK with user details and fake jwt token
                  let administrator = filteredAdmins[0];
                  let body = {
                      id: administrator.id,
                      username: administrator.username,
                      firstName: administrator.firstName,
                      lastName: administrator.lastName,
                      token: 'fake-jwt-token'
                  };

                  return of(new HttpResponse({ status: 200, body: body }));
              } else {
                  // else return 400 bad request
                  return throwError({ error: { message: 'Username or password is incorrect' } });
              }
          }
          // authentication for teacher
          if (request.url.endsWith('/teachers/authenticate') && request.method === 'POST') {
            // find if any user matches login credentials
            let filteredTeachers = teachers.filter(teacher => {
                return teacher.username === request.body.username && teacher.password === request.body.password;
            });

            if (filteredTeachers.length) {
                // if login details are valid return 200 OK with user details and fake jwt token
                let teacher = filteredTeachers[0];
                let body = {
                    id: teacher.id,
                    username: teacher.username,
                    firstName: teacher.firstName,
                    lastName: teacher.lastName,
                    token: 'fake-jwt-token'
                };

                return of(new HttpResponse({ status: 200, body: body }));
            } else {
                // else return 400 bad request
                return throwError({ error: { message: 'Username or password is incorrect' } });
            }
        }

            // get users
            if (request.url.endsWith('/users') && request.method === 'GET') {
                // tslint:disable-next-line:max-line-length
                // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    return of(new HttpResponse({ status: 200, body: users }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
              }

            // get activities
            if (request.url.endsWith('/activities') && request.method === 'GET') {
              // tslint:disable-next-line:max-line-length
              // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
              if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                  return of(new HttpResponse({ status: 200, body: activities }));
              } else {
                  // return 401 not authorised if token is null or invalid
                  return throwError({ status: 401, error: { message: 'Unauthorised' } });
              }
            }
            // get users
            if (request.url.endsWith('/admins') && request.method === 'GET') {
              // tslint:disable-next-line:max-line-length
              // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
              if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                  return of(new HttpResponse({ status: 200, body: admins }));
              } else {
                  // return 401 not authorised if token is null or invalid
                  return throwError({ status: 401, error: { message: 'Unauthorised' } });
              }
            }
            // get users
            if (request.url.endsWith('/teachers') && request.method === 'GET') {
              // tslint:disable-next-line:max-line-length
              // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
              if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                  return of(new HttpResponse({ status: 200, body: teachers }));
              } else {
                  // return 401 not authorised if token is null or invalid
                  return throwError({ status: 401, error: { message: 'Unauthorised' } });
              }
            }

            // get user by id
            if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
                // tslint:disable-next-line:max-line-length
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    let urlParts = request.url.split('/');
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    let matchedUsers = users.filter(user => { return user.id === id; });
                    let user = matchedUsers.length ? matchedUsers[0] : null;

                    return of(new HttpResponse({ status: 200, body: user }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
             // get user by id
             if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
              // tslint:disable-next-line:max-line-length
              // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
              if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                  // find user by id in users array
                  let urlParts = request.url.split('/');
                  let id = parseInt(urlParts[urlParts.length - 1]);
                  let matchedAdmins = admins.filter(administrator => { return administrator.id === id; });
                  let admin = matchedAdmins.length ? matchedAdmins[0] : null;

                  return of(new HttpResponse({ status: 200, body: admin }));
              } else {
                  // return 401 not authorised if token is null or invalid
                  return throwError({ status: 401, error: { message: 'Unauthorised' } });
              }
          }
           // get user by id
           if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
            // tslint:disable-next-line:max-line-length
            // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
            if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                // find user by id in users array
                let urlParts = request.url.split('/');
                let id = parseInt(urlParts[urlParts.length - 1]);
                let matchedTeachers = teachers.filter(teacher => { return teacher.id === id; });
                let teacher = matchedTeachers.length ? matchedTeachers[0] : null;

                return of(new HttpResponse({ status: 200, body: teacher }));
            } else {
                // return 401 not authorised if token is null or invalid
                return throwError({ status: 401, error: { message: 'Unauthorised' } });
            }
        }
            // get activity by id
            if (request.url.match(/\/activities\/\d+$/) && request.method === 'GET') {
              // tslint:disable-next-line:max-line-length
              // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
              if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                  // find user by id in users array
                  let urlParts = request.url.split('/');
                  let id = parseInt(urlParts[urlParts.length - 1]);
                  let matchedActivities = activities.filter(activity => { return activity.id === id; });
                  let activity = matchedActivities.length ? matchedActivities[0] : null;

                  return of(new HttpResponse({ status: 200, body: activity }));
              } else {
                  // return 401 not authorised if token is null or invalid
                  return throwError({ status: 401, error: { message: 'Unauthorised' } });
              }
          }

            // get user by username
            if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
              // tslint:disable-next-line:max-line-length
              // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
              if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                  // find user by id in users array
                  let urlParts = request.url.split('/');
                  let username = (urlParts['username']);
                  let matchedUsers = users.filter(user => { return user.username === username; });
                  let user = matchedUsers.length ? matchedUsers[0] : null;

                  return of(new HttpResponse({ status: 200, body: user }));
              } else {
                  // return 401 not authorised if token is null or invalid
                  return throwError({ status: 401, error: { message: 'Unauthorised' } });
              }
            }

            // register user
            if (request.url.endsWith('/users/register') && request.method === 'POST') {
                // get new user object from post body
                let newUser = request.body;

                // validation
                let duplicateUser = users.filter(user => { return user.username === newUser.username; }).length;
                if (duplicateUser) {
                    return throwError({ error: { message: 'Username "' + newUser.username + '" is already taken' } });
                }

                // save new user
                newUser.id = users.length + 1;
                users.push(newUser);
                localStorage.setItem('users', JSON.stringify(users));

                // respond 200 OK
                return of(new HttpResponse({ status: 200 }));
            }

            // register user
            if (request.url.endsWith('/admins/register') && request.method === 'POST') {
              // get new user object from post body
              let newAdmin = request.body;

              // validation
              let duplicateAdmin = admins.filter(administrator => { return administrator.username === newAdmin.username; }).length;
              if (duplicateAdmin) {
                  return throwError({ error: { message: 'Username "' + newAdmin.username + '" is already taken' } });
              }

              // save new user
              newAdmin.id = admins.length + 1;
              admins.push(newAdmin);
              localStorage.setItem('admins', JSON.stringify(admins));

              // respond 200 OK
              return of(new HttpResponse({ status: 200 }));
          }
          // register user
          if (request.url.endsWith('/teachers/register') && request.method === 'POST') {
            // get new user object from post body
            let newTeacher = request.body;

            // validation
            let duplicateTeacher = teachers.filter(teacher => { return teacher.username === newTeacher.username; }).length;
            if (duplicateTeacher) {
                return throwError({ error: { message: 'Username "' + newTeacher.username + '" is already taken' } });
            }

            // save new user
            newTeacher.id = teachers.length + 1;
            teachers.push(newTeacher);
            localStorage.setItem('teachers', JSON.stringify(teachers));

            // respond 200 OK
            return of(new HttpResponse({ status: 200 }));
        }
            // register activity
            if (request.url.endsWith('/activities/register') && request.method === 'POST') {
              // get new user object from post body
              let newActivity = request.body;

              // validation
              let duplicateActivity = users.filter(activity => { return activity.name === newActivity.name; }).length;
              if (duplicateActivity) {
                  return throwError({ error: { message: 'Name "' + newActivity.name + '" is already taken' } });
              }

              // save new user
              newActivity.id = activities.length + 1;
              activities.push(newActivity);
              localStorage.setItem('activities', JSON.stringify(activities));

              // respond 200 OK
              return of(new HttpResponse({ status: 200 }));
          }

            // // update user
            // if (request.url.match(/\/users\/\d+$/) && request.method === 'PATCH') {
            //   // get new user object from post body
            //   let newUser = request.body;
            //   let updateUser = users.findIndex(newUser.id);
            //   let updateUser = newUser;

            //   localStorage.setItem('users', JSON.stringify(updateUser));

            //   // respond 200 OK
            //   return of(new HttpResponse({ status: 200 }));
            // }


            // delete user
            if (request.url.match(/\/users\/\d+$/) && request.method === 'DELETE') {
                // tslint:disable-next-line:max-line-length
                // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                    // find user by id in users array
                    let urlParts = request.url.split('/');
                    // tslint:disable-next-line:radix
                    let id = parseInt(urlParts[urlParts.length - 1]);
                    for (let i = 0; i < users.length; i++) {
                        let user = users[i];
                        if (user.id === id) {
                            // delete user
                            users.splice(i, 1);
                            localStorage.setItem('users', JSON.stringify(users));
                            break;
                        }
                    }

                    // respond 200 OK
                    return of(new HttpResponse({ status: 200 }));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({ status: 401, error: { message: 'Unauthorised' } });
                }
            }
            // delete user
            if (request.url.match(/\/users\/\d+$/) && request.method === 'DELETE') {
              // tslint:disable-next-line:max-line-length
              // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
              if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                  // find user by id in users array
                  let urlParts = request.url.split('/');
                  // tslint:disable-next-line:radix
                  let id = parseInt(urlParts[urlParts.length - 1]);
                  for (let i = 0; i < teachers.length; i++) {
                      let teacher = teachers[i];
                      if (teacher.id === id) {
                          // delete user
                          teacher.splice(i, 1);
                          localStorage.setItem('teachers', JSON.stringify(teachers));
                          break;
                      }
                  }

                  // respond 200 OK
                  return of(new HttpResponse({ status: 200 }));
              } else {
                  // return 401 not authorised if token is null or invalid
                  return throwError({ status: 401, error: { message: 'Unauthorised' } });
              }
          }
          // delete user
          if (request.url.match(/\/users\/\d+$/) && request.method === 'DELETE') {
            // tslint:disable-next-line:max-line-length
            // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
            if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                // find user by id in users array
                let urlParts = request.url.split('/');
                // tslint:disable-next-line:radix
                let id = parseInt(urlParts[urlParts.length - 1]);
                for (let i = 0; i < admins.length; i++) {
                    let admin = admins[i];
                    if (admin.id === id) {
                        // delete user
                        admins.splice(i, 1);
                        localStorage.setItem('admins', JSON.stringify(admins));
                        break;
                    }
                }

                // respond 200 OK
                return of(new HttpResponse({ status: 200 }));
            } else {
                // return 401 not authorised if token is null or invalid
                return throwError({ status: 401, error: { message: 'Unauthorised' } });
            }
        }

            // delete activity
            if (request.url.match(/\/activities\/\d+$/) && request.method === 'DELETE') {
              // tslint:disable-next-line:max-line-length
              // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
              if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                  // find user by id in users array
                  let urlParts = request.url.split('/');
                  // tslint:disable-next-line:radix
                  let id = parseInt(urlParts[urlParts.length - 1]);
                  for (let i = 0; i < activities.length; i++) {
                      let activity = activities[i];
                      if (activity.id === id) {
                          // delete user
                          activities.splice(i, 1);
                          localStorage.setItem('activities', JSON.stringify(activities));
                          break;
                      }
                  }

                  // respond 200 OK
                  return of(new HttpResponse({ status: 200 }));
              } else {
                  // return 401 not authorised if token is null or invalid
                  return throwError({ status: 401, error: { message: 'Unauthorised' } });
              }
          }

            // pass through any requests not handled above
            return next.handle(request);

        }))

        // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
        .pipe(materialize())
        .pipe(delay(500))
        .pipe(dematerialize());
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
